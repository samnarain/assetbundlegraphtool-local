using UnityEngine;
using UnityEditor;


public class AbgtPreferences : EditorWindow
{
    private static bool prefsLoaded = false;

    // ABGT Settings
    public static string currentLoaderPath = "";
    public static int currentPanZoom = 0;
    public static bool currentClearCache = false;
    public static bool currentGenerateScripts = false;

    // GUI Styles
    static GUIStyle versionBoxStyle;
    static string versionAbgtOsx = "\nAssetBundleGraphTool - OSX version.\nRelease 1.0, custom fork from official tool.\nNo batteries included.\n";

    // Add "ABGT" section to Preferences
    [PreferenceItem("ABGT")]

    public static void PreferencesGUI()
    {

        // Load ABGT Preferences
        if (!prefsLoaded)
        {
            currentLoaderPath = EditorPrefs.GetString("currentLoaderPath", "");
            currentPanZoom = EditorPrefs.GetInt("currentPanZoom", 0);
            currentClearCache = EditorPrefs.GetBool("currentClearCache", false );
            currentGenerateScripts = EditorPrefs.GetBool("currentClearCache", false);
            prefsLoaded = true;
        }

        // Preferences Window Layout

        GUILayout.BeginVertical();
          GUILayout.Space(10);
          GUILayout.BeginHorizontal();
            GUILayout.Label(AssetDatabase.LoadAssetAtPath<Texture> ("Assets/AssetBundleGraph/Editor/OSX/images/abgt-circle.png"));
            if(EditorGUIUtility.isProSkin) {
              versionBoxStyle = new GUIStyle(GUI.skin.label);
              versionBoxStyle.normal.textColor = Color.white;
              GUILayout.Label(versionAbgtOsx, versionBoxStyle, GUILayout.ExpandWidth(true), GUILayout.Height(64));
            } else {
              GUILayout.Label(versionAbgtOsx, GUILayout.ExpandWidth(true), GUILayout.Height(64));
            }

        GUILayout.EndVertical();

        GUILayout.BeginVertical();

          GUILayout.Space(10);
          GUILayout.BeginHorizontal();
            GUILayout.Label( new GUIContent("Default loader path", "Define your default root for loading assets for the loader node."), GUILayout.MaxWidth(150));
            currentLoaderPath = GUILayout.TextField (currentLoaderPath, 255, GUILayout.MaxWidth(200));
          GUILayout.EndHorizontal();
          GUILayout.Space(10);
          GUILayout.BeginHorizontal();

          string[] options = new string[]
          {
              "Original", "Natural", "Professional"
          };
          GUILayout.Label( new GUIContent("Pan/Zoom Settings", "Select your preferred settings for Pan/Zoom"), GUILayout.MaxWidth(150));
          currentPanZoom = EditorGUILayout.Popup(currentPanZoom, options, GUILayout.MaxWidth(200));
          GUILayout.EndHorizontal();
          GUILayout.Space(10);

        GUILayout.EndVertical();

        GUILayout.BeginVertical();

          GUILayout.Space(10);
          GUILayout.BeginHorizontal();
            GUILayout.Label( new GUIContent("Cleanup after build", "Clear caches automatically after every build"), GUILayout.MaxWidth(150));
            currentClearCache = GUILayout.Toggle(currentClearCache, "", GUILayout.MaxWidth(200));
          GUILayout.EndHorizontal();
          GUILayout.BeginHorizontal();
            GUILayout.Label( new GUIContent("Generate CUI scripts", "Generate commandline scripts after every build"), GUILayout.MaxWidth(150));
            currentGenerateScripts = GUILayout.Toggle(currentGenerateScripts, "", GUILayout.MaxWidth(200));
          GUILayout.EndHorizontal();
          GUILayout.Space(10);

        GUILayout.EndVertical();

        GUILayout.BeginVertical();

          GUILayout.Space(20);
          GUILayout.BeginHorizontal();
            GUILayout.Button (new GUIContent (" Clean now ", "Empty the cache now"));
            GUILayout.Button (new GUIContent (" Build all ", "Build all ABGT graphs"));
          GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.BeginVertical();

          GUILayout.Space(40);
          GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button (new GUIContent (" Default settings ", "Changes the current settings back to the default state"))) {

              currentLoaderPath = "";
              currentPanZoom = 0;
              currentClearCache = false;
              currentGenerateScripts = false;

            }
          GUILayout.EndHorizontal();
          GUILayout.Space(10);

        GUILayout.EndVertical();

        // Save ABGT preferences
        if (GUI.changed) {
          EditorPrefs.SetString("currentLoaderPath", currentLoaderPath);
          EditorPrefs.SetInt("currentPanZoom", currentPanZoom);
          EditorPrefs.SetBool("currentClearCache", currentClearCache );
          EditorPrefs.SetBool("currentClearCache", currentGenerateScripts);
        }

    }
}
